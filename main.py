import pyodbc
import datetime
from typing import List, Dict, Sequence

from dotenv import dotenv_values
config = dotenv_values('.env')

conn = pyodbc.connect(
    Trusted_Connection='yes', 
    driver='{SQL Server}',
    server=config['SERVER'],
    database=config['DATABASE'],
)

def get_db_version(cur: pyodbc.Cursor) -> None:
    cur.execute("SELECT @@VERSION")
    while True:
        row = cur.fetchone()
        if not row: break
        print(row)

def create_table_authors(cur: pyodbc.Cursor) -> None:
    query = '''
CREATE TABLE authors 
(
    id INT PRIMARY KEY IDENTITY(1, 1),
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    birthdate DATE NOT NULL,
    added DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
)
'''
    try:
        cur.execute(query)
    except Exception as e:
        print(f'[Error]: {e}')


def fill_table_author(cur: pyodbc.Cursor) -> None:
    import json
    author_insert_query = '''
INSERT INTO authors (first_name, last_name, email, birthdate)
VALUES (?, ?, ?, ?)
'''
    book_insert_query = '''
INSERT INTO books (title, excerpt, author_id)
VALUES (
    ?, 
    ?,
    (SELECT id FROM authors WHERE email=?)
)
'''
    try:
        with open('./books.json', 'r') as f:
            books: List[Dict[str, str | int]] = json.loads(f.read())
            author_names: Sequence = list(map(lambda book: book['author'], books))
            authors = []
            for author in author_names:
                first_name = ''.join(author.split(' ')[0:-1])
                last_name = author.split(' ')[-1]
                email = f'{first_name}@mail.com'
                birthdate = datetime.datetime.now()
                authors.append([first_name, last_name, email, birthdate])

            cur.executemany(author_insert_query, authors)

            for book in books:
                first_name = ''.join(book['author'].split(' ')[0:-1])
                email = f'{first_name}@mail.com'
                cur.execute(book_insert_query, book['title'], book['description'], email)


    except FileExistsError:
        print("[Error]: File authors.txt does not exists")
    except Exception as e:
        print(f"[Error]: {e}")

with conn:
    cur = conn.cursor()
    get_db_version(cur)
    # create_table_authors(cur)
    fill_table_author(cur)
